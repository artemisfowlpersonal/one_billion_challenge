# one_billion_challenge
Yet another solution to the one billion challenge, this time yet again written in Rust. This solution focuses heavily on single core performance.

## Installation
To install, you must use the nightly toolchain from Rust. See [here](https://www.rust-lang.org/tools/install) on a guide for the installation of rust and [here](https://rust-lang.github.io/rustup/concepts/channels.html) on the installation of nightly.
```
git pull https://gitlab.com/artemisfowlpersonal/one_billion_challenge/
cd one_billion_challenge
make run
```

## Usage
Note: All make commands assume that the data file is located under "data/measurements.txt". \
`make generate` creates the necessary files for running. \
`make run` executes the binary. \
`make build` builds the binary. \
`make pgo` builds the binary using profile guided optimizations. \
`make time` times a single run of the binary. \
`make hyperfine` times ten runs of the binary. \
`cargo run -r -- <PATH>` executes the binary. The default for PATH is also "data/measurements.txt".

