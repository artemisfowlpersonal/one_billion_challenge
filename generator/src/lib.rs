use indicatif::ProgressBar;
use rand::{prelude::*, thread_rng};
use rand_distr::Normal;
use std::fs::File;
use std::io::{BufWriter, Write};
use std::path::Path;

// TODO: Feature flag deciding whether to panic or just exit process?

fn generate_unique_entries(input: &Path, unique_entries: usize) -> Vec<(String, f32)> {
    let mut rng = thread_rng();

    let Ok(input) = std::fs::read_to_string(input) else {
        eprintln!("Could not read input file {input:?}. Is it valid?");
        std::process::exit(0);
    };

    input
        .lines()
        // TODO: Nicer output
        .map(|line| line.rsplit_once(';').expect("No separator found"))
        // TODO: Nicer output
        .map(|(name, temperature)| {
            (
                name.to_string(),
                temperature.parse().expect("Invalid number"),
            )
        })
        .choose_multiple(&mut rng, unique_entries)
}

pub fn generate_file<I: AsRef<Path>, O: AsRef<Path>>(
    size: usize,
    unique_entries: usize,
    deviation: f32,
    input: I,
    output_path: O,
    quiet_mode: bool,
) {
    if unique_entries == 0 {
        eprintln!("Number of unique entries is set to 0. Please set higher.");
        std::process::exit(1);
    }

    let input = input.as_ref();
    let output_path = output_path.as_ref();
    let output = match File::create(output_path) {
        Ok(file) => file,
        Err(e) => {
            eprintln!("Could not create output file. Exact error {e}.");
            std::process::exit(1);
        }
    };
    let mut output = BufWriter::new(output);

    let bar = if quiet_mode {
        ProgressBar::hidden()
    } else {
        ProgressBar::new(size as u64)
    };

    let entries = generate_unique_entries(input, unique_entries);
    if entries.is_empty() {
        eprintln!("Number of unique entries is 0. Does the input file contain parsable content?");
        std::process::exit(1);
    }
    let mut rng = thread_rng();
    if !quiet_mode {
        bar.println(format!("Starting with file {:?}", output_path.as_os_str()));
    }

    for _ in bar.wrap_iter(0..size) {
        let (name, temperature) = entries.choose(&mut rng).expect("Entry size is 0");
        // Standard deviation is not infinite
        let distribution = Normal::new(*temperature, deviation).unwrap();
        let temperature = distribution.sample(&mut rng)
            .clamp(-99.9, 99.9);

        writeln!(output, "{name};{:.1}", temperature)
            .expect("Could not write to file");
    }

    bar.finish();
}
