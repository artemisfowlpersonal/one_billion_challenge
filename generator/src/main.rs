use clap::Parser;
use generator::generate_file;
use std::path::PathBuf;

#[derive(Parser, Debug)]
#[command()]
struct Args {
    /// The amount of entries in the output file
    //#[arg(short, long, default_value_t = 1_000_000_000)]
    #[arg(default_value_t = 1_000_000_000)]
    count: usize,
    /// The amount of unique entries that appear in the output file
    #[arg(long, default_value_t = 10_000)]
    unique_entries: usize,
    /// The input file to source the cities and average temperatures from. Must be a csv file
    #[arg(short, long)]
    input: PathBuf,
    /// The output file where the entries are stored in
    #[arg(short, long, default_value = "measurements.txt")]
    output: PathBuf,
    /// The standard deviation from the average per entry.
    #[arg(short, long, default_value_t = 10.0)]
    deviation: f32,
    #[arg(short, long)]
    quiet: bool,
}

fn main() {
    let args = Args::parse();
    generate_file(
        args.count,
        args.unique_entries,
        args.deviation,
        args.input,
        args.output,
        args.quiet,
    );
}
