use std::fmt::{self, Display};

use float_eq::float_eq;
use hashbrown::HashMap;

use super::entry;

#[derive(Clone, Copy, Debug, Default)]
struct WeatherStation {
    max: f32,
    min: f32,
    mean: f32,
    count: usize,
}
impl WeatherStation {
    fn new() -> Self {
        Default::default()
    }

    fn add(&mut self, item: f32) {
        if self.count != 0 {
            self.max = self.max.max(item);
            self.min = self.min.min(item);
        } else {
            self.max = item;
            self.min = item;
        }
        let prev_count = self.count;
        self.count += 1;
        self.mean = self.mean.mul_add(prev_count as f32, item) / self.count as f32;
    }
}
impl PartialEq<entry::BufferedEntry> for WeatherStation {
    fn eq(&self, other: &entry::BufferedEntry) -> bool {
        let entry = other.entry();
        other.is_empty()
            && self.count == entry.count as usize
            && float_eq!(self.max, f32::from(entry.max) / 10.0, abs <= 0.1)
            && float_eq!(self.min, f32::from(entry.min) / 10.0, abs <= 0.1)
            && float_eq!(self.mean, f32::from(entry.mean) / 10.0, abs <= 0.1)
    }
}
impl Display for WeatherStation {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:.1}/{:.1}/{:.1}", self.max, self.min, self.mean)
    }
}

pub fn parse_and_compare(input: &str, compare: crate::Entries) {
    let mut entries = HashMap::with_capacity(10_000);
    input
        .lines()
        .map(|line| line.rsplit_once(';').expect("No delimiter found"))
        // As this is only intended for tests, unwrap is fine
        .map(|(name, temperature)| (name, temperature.parse().unwrap()))
        .for_each(|(name, temperature)| {
            entries
                .entry(name)
                .or_insert_with(WeatherStation::new)
                .add(temperature);
        });

    assert_eq!(entries.len(), compare.entries.len());
    assert_eq!(entries.len(), compare.indexes.len());

    for (name, correct_station) in entries {
        let cmp_index = compare
            .indexes
            .get(name)
            .unwrap_or_else(|| panic!("Could not find {name}"));
        let cmp_station = &compare.entries[*cmp_index];

        assert_eq!(
            correct_station, *cmp_station,
            "Data for entry {name} is {cmp_station} but should be {correct_station}"
        );
    }
}
