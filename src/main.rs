use std::fs::File;

use memmap2::Mmap;
use one_billion_challenge::parse_unchecked;

#[doc(hidden)]
fn main() {
    let path = std::env::args()
        .nth(1)
        .unwrap_or_else(|| "data/measurements.txt".to_owned());

    // As you can see, undefined behaviour rocks...
    let file = File::open(path).unwrap();
    // SAFETY: Surely we will not change the file during operation? Surely?
    let mapped_data = unsafe { Mmap::map(&file).unwrap() };

    // SAFETY: Input data should hopefully be well formed. If not? Ehh, users fault.
    println!("{}", unsafe { parse_unchecked(&mapped_data) });
}
