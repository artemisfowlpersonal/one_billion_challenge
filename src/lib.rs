#![feature(portable_simd)]
#![feature(test)]
#![feature(stdarch_x86_avx512)]

extern crate test;
pub mod entry;
pub mod parse;

macro_rules! checked_assert {
    ($cond:expr, $($arg:tt)+) => {
        #[cfg(not(feature = "no-checks"))]
        assert!($cond, $($arg)*);
        #[cfg(feature = "no-checks")]
        debug_assert!($cond, $($arg)*);
    }
}

use core::{
    fmt::{self, Display},
    hash::BuildHasherDefault,
};

use entry::BufferedEntry;
use gxhash::GxHasher;
use hashbrown::{hash_map::Entry, HashMap};
use rpmalloc::RpMalloc;
pub(crate) use checked_assert;

#[global_allocator]
#[doc(hidden)]
static ALLOCATOR: RpMalloc = RpMalloc;

// TODO: It would be lovely if it was possible to use f16 floats. Even if it is just to test.
// Parsing the numbers would very likely be a lot slower,
// however the simd intrinsics might be more efficient?
// Also, the cpu that is used to test thiss does not have any native f16 support.
// Parsing could perhaps use https://doc.rust-lang.org/core/arch/x86_64/fn._mm512_cvt_roundepi16_ph.html
//
// FIXME: Inaccuracies in new test case. Figure out if more than rounding error

// TODO: Major time loss is due to comparisons of the hashes
#[doc(hidden)]
type WeatherHashMap<'a> = HashMap<&'a str, usize, BuildHasherDefault<GxHasher>>;
#[doc(hidden)]
const ENTRIES_SIZE_APPROXIMATOR: usize = 10000;
#[doc(hidden)]
const HASHMAP_SIZE_APPROXIMATOR: usize = 5 * ENTRIES_SIZE_APPROXIMATOR;

/// Creates weatherstation entries by parsing the input slice.
/// # Examples
/// ```
/// # use one_billion_challenge::{parse_unchecked, Entries};
/// let input = b"Tokio;12.3
/// Berlin;4.8
/// Tokio;-5.3";
/// let output: Entries<'_> = unsafe { parse_unchecked(input) };
/// assert_eq!(
///     output.to_string(),
///     "{Berlin=4.8/4.8/4.8, Tokio=12.3/-5.3/3.5}",
/// );
/// ```
/// # Safety
/// The given file must either be empty or have a minimum size of 5 bytes.
/// Each line (marked by an ASCII '\n' byte at the end, or the end of the file) must contain an
/// ASCII ';' byte in the four bytes before the last byte.
/// Each line also must be at least five bytes and at maximum 64 bytes long.
///
/// The slice after the separator until the newline (marked by ASCII ';') must be three or four
/// bytes long. An exception to this is if it starts with an ASCII '-' byte, in which case it must
/// be four or fives bytes long.
///
/// The slice from the newline until the separator must contain valid UTF8.
///
/// The amount of added entries should never reach 0.
#[inline]
#[must_use]
pub unsafe fn parse_unchecked(input: &[u8]) -> Entries<'_> {
    let mut indexes =
        HashMap::with_capacity_and_hasher(HASHMAP_SIZE_APPROXIMATOR, BuildHasherDefault::default());
    let mut entries = Vec::with_capacity(ENTRIES_SIZE_APPROXIMATOR);

    // TODO: Maybe multithread by collecting into large buffers (like 20k elements) and then
    // processing each one TODO: Or in general, load a piece into memory and then process it

    let mut remainder = input.strip_suffix(b"\n").unwrap_or(input);

    while !remainder.is_empty() {
        // SAFETY: It is up to the caller that if this loop is entered, the file is large enough.
        // SAFETY: It is up to the caller that each line is between 5 and 64 bytes, contains a
        // separator and a newline.
        let (name, data, r) = unsafe { parse::full_split(remainder) };
        remainder = r;

        // SAFETY: It is up to the caller to ensure that the separators are correctly placed
        let data = parse::fixed_ascii_int_to_decimal(data);
        match indexes.entry(name) {
            Entry::Occupied(entry) => {
                let index = entry.get();
                // SAFETY: This is safe because we know that only previous lengths are stored.
                // As entries only grows, this thus means that we will always be in bounds.
                let entry: &mut BufferedEntry = unsafe { entries.get_unchecked_mut(*index) };

                // SAFETY:It is up to the caller to ensure that the amount of entries does not
                // overflow until 0
                unsafe { entry.add_entry(data) };
            }
            Entry::Vacant(v) => {
                v.insert(entries.len());
                entries.push(BufferedEntry::new(data));
            }
        };
    }

    for value in &mut entries {
        // SAFETY:It is up to the caller to ensure that the amount of entries does not overflow
        // until 0
        unsafe {
            value.add_buffered_entries();
        }
    }
    Entries { indexes, entries }
}

/// All the accumulated entries of a file.
#[derive(Debug)]
pub struct Entries<'a> {
    indexes: WeatherHashMap<'a>,
    entries: Vec<BufferedEntry>,
}
impl Display for Entries<'_> {
    #[allow(clippy::inline_always)]
    #[inline(always)]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut iter = self
            .indexes
            .iter()
            // SAFETY: This is safe because we know that only previous lengths are stored.
            // As entries only grows, this thus means that we will always be in bounds.
            // Check in `parse_unchecked` where we push new entries for that assumption.
            .map(|(key, index)| (key, unsafe { self.entries.get_unchecked(*index) }));
        let (key, entry) = iter.next().unwrap();
        let _ = write!(f, "{{");
        iter.fold(&mut *f, |output, (key, entry)| {
            let _ = write!(output, "{key}={entry:.1}, ");
            output
        });
        write!(f, "{key}={entry:-1}}}")
    }
}

#[cfg(test)]
mod tests {
    use std::fs::{read_to_string, File};

    use memmap2::Mmap;
    use test::{black_box, Bencher};

    use super::*;

    mod dumb;

    const TEST_INPUT_LOCATION1: &str = "data/test_measurements.txt";
    const HANDPICKED_INPUT_LOCATION1: &str = "data/test_measurements1.txt";
    const HANDPICKED_INPUT_LOCATION2: &str = "data/test_measurements2.txt";
    const BENCH_INPUT_LOCATION: &str = "data/bench_measurements.txt";

    #[test]
    fn build_tests() {
        let file = File::open(TEST_INPUT_LOCATION1).unwrap();
        // SAFETY: Test data hopefully isn't modified during testing
        let mapped_data = unsafe { Mmap::map(&file).unwrap() };
        // SAFETY: Input data should hopefully be well formed. If not? Ehh, users fault.
        let entries = unsafe { parse_unchecked(&mapped_data) };
        let input = read_to_string(TEST_INPUT_LOCATION1).unwrap();
        dumb::parse_and_compare(&input, entries);
    }

    #[test]
    fn parse_tests() {
        let file = File::open(HANDPICKED_INPUT_LOCATION1).unwrap();
        // SAFETY: Test data hopefully isn't modified during testing
        let mapped_data = unsafe { Mmap::map(&file).unwrap() };
        // SAFETY: Input data should hopefully be well formed. If not? Ehh, users fault.
        let entries = unsafe { parse_unchecked(&mapped_data) };
        let input = read_to_string(HANDPICKED_INPUT_LOCATION1).unwrap();
        dumb::parse_and_compare(&input, entries);
    }

    #[test]
    fn parse_tests2() {
        let file = File::open(HANDPICKED_INPUT_LOCATION2).unwrap();
        // SAFETY: Test data hopefully isn't modified during testing
        let mapped_data = unsafe { Mmap::map(&file).unwrap() };
        // SAFETY: Input data should hopefully be well formed. If not? Ehh, users fault.
        let entries = unsafe { parse_unchecked(&mapped_data) };
        let input = read_to_string(HANDPICKED_INPUT_LOCATION2).unwrap();
        dumb::parse_and_compare(&input, entries);
    }

    #[bench]
    fn bench_build_speed(b: &mut Bencher) {
        let file = File::open(BENCH_INPUT_LOCATION).unwrap();
        // SAFETY: Test data hopefully isn't modified during testing
        let mapped_data = unsafe { Mmap::map(&file).unwrap() };
        // SAFETY: Input data should hopefully be well formed. If not? Ehh, users fault.
        let entries = unsafe { parse_unchecked(&mapped_data) };
        let input = read_to_string(BENCH_INPUT_LOCATION).unwrap();
        dumb::parse_and_compare(&input, entries);

        b.iter(|| {
            let file = File::open(BENCH_INPUT_LOCATION).unwrap();
            // SAFETY: Test data hopefully isn't modified during testing
            let mapped_data = unsafe { Mmap::map(&file).unwrap() };
            // SAFETY: Input data should hopefully be well formed. If not? Ehh, users fault.
            black_box(unsafe { parse_unchecked(black_box(&mapped_data)) });
        });
    }
}
