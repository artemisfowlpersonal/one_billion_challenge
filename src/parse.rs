use std::{
    hint,
    simd::{
        cmp::SimdPartialEq,
        num::{SimdInt, SimdUint},
        Simd,
    },
};

#[cfg(all(target_feature = "avx512bw", feature = "avx512-loader"))]
#[inline]
unsafe fn load(bytes: &[u8]) -> Simd<u8, 64> {
    use std::arch::x86_64::_mm512_loadu_epi8;
    // Not 100% sure if this behaviour is actually fine and not undefined
    let buffer = _mm512_loadu_epi8(bytes.as_ptr().cast());
    buffer.into()
}
#[cfg(not(all(
    target_feature = "avx512bw", 
    feature = "avx512-loader"
)))]
#[inline]
unsafe fn load(bytes: &[u8]) -> Simd<u8, 64> {
    #[inline]
    #[cold]
    fn cold() {}

    #[inline]
    fn unlikely(b: bool) -> bool {
        if b {
            cold()
        }
        b
    }

    if unlikely(bytes.len() < 64) {
        let mut buffer = [0; 64];
        buffer[..bytes.len()].copy_from_slice(bytes);
        buffer
    } else {
        let buffer = &bytes[..64];
        let buffer = buffer.try_into();
        // SAFETY: We have checked that we have 64 bytes
        unsafe { buffer.unwrap_unchecked() }
    }
    .into()
}

/// Quickly parses a corresponding input of form b"name;data\nremainder" into "name", b"data" and
/// b"remainder".
///
/// # Safety
/// * The input slice must have a length of at least five and less than 64.
/// * The input must be proper utf8 before the ';' separator.
/// * The input must contain a ';' byte as separator in the last four bytes before the last byte.
/// * The data section must be at least four bytes long.
///
/// # Examples
/// ```
/// # use one_billion_challenge::parse::full_split;
/// let input = b"Puerto Rico;-48.2";
/// let output: (&str, &[u8], &[u8]) = ("Puerto Rico", b"-48.2", b"");
/// assert_eq!(unsafe { full_split(input) }, output,);
/// ```
/// The remainder is everything after the newline
/// ```
/// # pub use one_billion_challenge::parse::full_split;
/// let input = b"Tokio;10.2\nREMAINDER";
/// let output: (&str, &[u8], &[u8]) = ("Tokio", b"10.2", b"REMAINDER");
/// assert_eq!(unsafe { full_split(input) }, output,);
/// ```
#[inline]
#[must_use]
pub unsafe fn full_split(bytes: &[u8]) -> (&str, &[u8], &[u8]) {
    let buffer = load(bytes);

    let mask = buffer.simd_eq(Simd::splat(b';'));
    super::checked_assert!(
        mask.first_set().is_some(),
        "{} contains no separator ';'.",
        std::str::from_utf8(buffer.as_array()).unwrap()
    );
    // SAFETY: It is up the caller to ensure there is a separator
    let separator_pos = unsafe { mask.first_set().unwrap_unchecked() };

    // SAFETY: It is up to the caller to ensure that the data section is long enough
    let newline_pos = unsafe {
        let minus_offset = usize::from(bytes.get_unchecked(separator_pos + 1) == &b'-');
        let long_offset =
            usize::from(bytes.get_unchecked(separator_pos + 2 + minus_offset) != &b'.');
        separator_pos + 4 + minus_offset + long_offset
    };
    // TODO: Test impact of this (might acually even be positive???)
    //et newline_pos = newline_pos.min(bytes.len());

    // SAFETY: Since there is a separator in bounds, this will be in bounds
    let name = unsafe { bytes.get_unchecked(..separator_pos) };
    // SAFETY: Since there is a newline in bounds, this will be in bounds
    let data = unsafe { bytes.get_unchecked(separator_pos + 1..newline_pos) };

    // Check if we are at the end of the slice
    let newline_offset = usize::from(newline_pos != bytes.len());
    // SAFETY: Taking an empty slice is valid
    let remaining = unsafe { bytes.get_unchecked(newline_pos + newline_offset..) };

    // SAFETY: As the divider is guaranteed to be on ';', the corresponding strings should always be
    // on a char border
    // SAFETY: Additionally we have the guarantee that this part is valid utf8
    let name = unsafe { std::str::from_utf8_unchecked(name) };

    (name, data, remaining)
}

/// Reads a three or four byte slice decimal number and converts it into a 4 byte array without the
/// second to last byte. If the length is three bytes, the remaining bit is set to ASCII '0'. The
/// order of the bytes is reversed. # Safety
/// The length of the input slice must be either three or four bytes.
/// # Examples
/// ```ignore
/// let input = &[0, 5, 3];
/// assert_eq!(unsafe { to_buffer(input) }, [3, 0, 48, 48])
/// ```
/// ```ignore
/// let input = &[12, 9, 6, 3];
/// assert_eq!(unsafe { to_buffer(input) }, [3, 6, 12, 48])
/// ```
/// ```ignore
/// let input = b"12.4";
/// assert_eq!(unsafe { to_buffer(input) }, [b'4', b'2', b'1', b'0'])
/// ```
#[allow(clippy::cast_possible_truncation)]
#[inline]
unsafe fn to_buffer(bytes: &[u8]) -> [u8; 4] {
    let len = bytes.len();
    super::checked_assert!(
        (3..=4).contains(&len),
        "buffer length is {len}, when it should be between 3 and 4"
    );
    // LLVM is not able to see that this is (3..=4).contains(&len).
    // As such the result would be worse if we follow clippy
    // SAFETY: It is up to the caller the length is either 3 or 4
    #[allow(clippy::manual_range_contains)]
    hint::assert_unchecked(len >= 3 && len <= 4);

    let mut num_part = [b'0'; 4];
    num_part[0] = bytes[len - 1];
    num_part[1] = bytes[len - 3];
    num_part[2] = (bytes[0] * (len as u8 - 3)) | b'0';

    num_part
}

/// Parses a byte slice containing ASCII number literals into a number
/// # Safety
/// The length of the input slice must be either three or four bytes.
/// # Examples
/// ```ignore
/// let input = b"34.1";
/// assert_eq!(unsafe { fast_parse_helper(input) }, 341);
/// ```
#[inline]
unsafe fn fast_parse_helper(bytes: &[u8]) -> i16 {
    // SAFETY: It is up to the caller the length is either 3 or 4
    let num_part = to_buffer(bytes);

    let mut buffer = Simd::from_array(num_part);
    buffer -= Simd::splat(b'0');
    let mut buffer: Simd<i16, 4> = buffer.cast();
    buffer *= Simd::from_array([1, 10, 100, 0]);
    buffer.reduce_sum()
}

/// Quickly parses a floating point number represented as a byte slice into a decimal representation
/// of i16.
///
/// # Safety
/// The length of the input slice must be either three or four bytes, unless the slice starts with a
/// ASCII '-'. In that case the slice must be four or five bytes long. # Examples
/// ```
/// # use one_billion_challenge::parse::fixed_ascii_int_to_decimal;
/// let input = b"34.1";
/// assert_eq!(unsafe { fixed_ascii_int_to_decimal(input) }, 341);
/// ```
/// # Examples
/// ```
/// # use one_billion_challenge::parse::fixed_ascii_int_to_decimal;
/// let input = b"-92.3";
/// assert_eq!(unsafe { fixed_ascii_int_to_decimal(input) }, -923);
/// ```
#[inline]
#[must_use]
pub unsafe fn fixed_ascii_int_to_decimal(input: &[u8]) -> i16 {
    super::checked_assert!(
        (3..5).contains(&input.len()) || (5 == input.len() && input[0] == b'-'),
        "the length of the integer part {} to be parsed is {}, not 4",
        std::str::from_utf8(input).unwrap(),
        input.len(),
    );
    // Strictly not necessary, but nice for debugging anyways
    #[cfg(debug_assertions)]
    for (i, &byte) in input.iter().enumerate().rev() {
        super::checked_assert!(
            byte.is_ascii_digit()
                || (byte == b'.' && i == input.len() - 2)
                || (byte == b'-' && i == 0),
            "input contains not accepted byte {:?} at position {}",
            byte as char,
            i
        );
    }

    let is_negative = input.get_unchecked(0) >> 4 ^ 3;
    // SAFETY: It is up to the caller the length is either 3 or 4, ignoring the '-' byte
    let result = fast_parse_helper(input.get_unchecked(is_negative as usize..));
    let is_negative = i16::from(is_negative);
    (result ^ (i16::MAX * is_negative))
        .wrapping_add(i16::MAX * is_negative)
        .wrapping_add(2 * is_negative)
}
