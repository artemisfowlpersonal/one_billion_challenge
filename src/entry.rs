use std::fmt::{self, Display};

use arrayvec::ArrayVec;

// TODO: Experiment with larger sizes
/// How many entries can be buffered.
const BUFFER_CAPACITY: usize = 1024;

/// A weather station entry with a buffer to allow for infrequent batch processing.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct BufferedEntry {
    buffer: ArrayVec<i16, BUFFER_CAPACITY>,
    entry: WeatherStation,
}
impl BufferedEntry {
    /// Creates a new weather station entry by creating a new weatherstation and initializing the
    /// buffer.
    #[inline]
    #[must_use]
    pub fn new(entry: i16) -> Self {
        Self {
            buffer: ArrayVec::new(),
            entry: WeatherStation::new(entry),
        }
    }

    /// Adds the given entry to the buffer. If the buffer overflows, adds them to the weatherstation
    /// entry
    /// # Safety
    /// The amount of added entries should never reach 0.
    #[inline]
    pub unsafe fn add_entry(&mut self, item: i16) {
        if self.buffer.try_push(item).is_err() {
            // Hopefully compiler autovectorizes
            self.add_buffered_entries();
            // Will only panic when buffer size is 0, which it isn't
            self.buffer.push(item);
        }
    }

    /// Empties the buffer and adds the entries to the weatherstation entry
    /// # Safety
    /// The amount of added entries should never reach 0.
    #[inline]
    pub unsafe fn add_buffered_entries(&mut self) {
        // SAFETY:It is up to the caller to ensure that the amount of entries does not overflow
        // until 0
        unsafe {
            self.entry.add_many(&self.buffer);
        }
        // SAFETY: Setting length to 0 is always legal
        unsafe {
            self.buffer.set_len(0);
        }
    }

    #[cfg(test)]
    #[must_use]
    pub const fn entry(&self) -> &WeatherStation {
        &self.entry
    }

    #[cfg(test)]
    #[must_use]
    pub const fn is_empty(&self) -> bool {
        self.buffer.is_empty()
    }
}
impl Display for BufferedEntry {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.entry.fmt(f)
    }
}
impl From<f32> for BufferedEntry {
    fn from(value: f32) -> Self {
        Self {
            buffer: ArrayVec::new(),
            entry: value.into(),
        }
    }
}
impl From<WeatherStation> for BufferedEntry {
    fn from(entry: WeatherStation) -> Self {
        Self {
            buffer: ArrayVec::new(),
            entry,
        }
    }
}

/// The basic weather station entry
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct WeatherStation {
    /// The maximum possible temperature a weatherstation has recorded.
    pub max: i16,
    /// The minimum possible temperature a weatherstation has recorded.
    pub min: i16,
    /// The average temperature a weatherstation has recorded.
    pub mean: i16,
    /// How many times the weatherstation has been mentioned.
    pub count: i16,
}
impl WeatherStation {
    /// Creates a new weather station entry by setting the count to 1 and everything else to the
    /// given entry.
    #[inline]
    #[must_use]
    pub const fn new(entry: i16) -> Self {
        Self {
            max: entry,
            min: entry,
            mean: entry,
            count: 1,
        }
    }

    /// Adds a collection of items to the entry according to the following:
    /// * This function recalculates the mean to be over all elements.
    /// * The count the previous count plus item length.
    /// * The min and max are the min and max over the collection compared to the current.
    ///
    /// The operation is a no op when len of items is 0.
    ///
    /// # Safety
    /// The amount of added entries should never reach 0.
    ///
    /// # Examples
    /// ```
    /// # use one_billion_challenge::entry::WeatherStation;
    /// let mut station = WeatherStation::new(50);
    /// unsafe { station.add_many(&[10, 22, 303]) };
    /// assert_eq!(
    ///     station,
    ///     WeatherStation {
    ///         max: 303,
    ///         min: 10,
    ///         mean: 96,
    ///         count: 4,
    ///     }
    /// );
    /// ```
    #[allow(clippy::cast_possible_truncation)]
    #[allow(clippy::cast_possible_wrap)]
    #[inline]
    pub unsafe fn add_many(&mut self, items: &[i16]) {
        let count = i32::from(self.count);
        self.count += items.len() as i16;
        super::checked_assert!(
            self.count != 0,
            "division by 0 would have occured in add_many",
        );

        // TODO: https://doc.rust-lang.org/core/arch/x86_64/fn._mm512_avg_epu16.html
        // https://math.stackexchange.com/questions/2091521/how-do-i-calculate-a-weighted-average-from-two-averages
        let mean: i32 = (i32::from(self.mean) * count
            + items.iter().copied().map(i32::from).sum::<i32>())
        .checked_div(i32::from(self.count))
        .unwrap_unchecked();
        self.mean = mean as i16;
        self.min = *items.iter().min().unwrap_or(&self.min).min(&self.min);
        self.max = *items.iter().max().unwrap_or(&self.max).max(&self.max);
    }
}
impl From<f32> for WeatherStation {
    #[allow(clippy::cast_possible_truncation)]
    fn from(value: f32) -> Self {
        let value = (value * 10.0) as i16;
        Self {
            max: value,
            min: value,
            mean: value,
            count: 1,
        }
    }
}
impl Display for WeatherStation {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let max = f32::from(self.max) / 10.0;
        let min = f32::from(self.min) / 10.0;
        let mean = f32::from(self.mean) / 10.0;
        write!(f, "{max:.1}/{min:.1}/{mean:.1}")
    }
}
