SHELL := /bin/bash
BUILD_FLAGS := -Ctarget-cpu=native -Clink-arg=-fuse-ld=mold
CLIPPY_ARGS:= -W clippy::all -W clippy::correctness -W clippy::complexity -W clippy::style -W clippy::pedantic -W clippy::nursery -W clippy::suspicious -W clippy::nursery --W clippy::perf
MEASUREMENT := $(shell ls data/*.txt)
GENERATOR_PATH := ./target/release/generator

.display_finish_time:
	@echo "Finished at: $(shell date +%d.%m.%Y" "%H:%M:%S)" 

check:
	@cargo +nightly check --profile=testing

build:
	@cargo +nightly build --release

run:
	@cargo +nightly run --release

test:
	@cargo +nightly test --profile=testing

bench:
	@cargo +nightly bench --profile=release

clippy:
	@cargo +nightly clippy --profile=testing -- ${CLIPPY_ARGS}

clippy-fix:
	@cargo +nightly clippy  --fix --allow-dirty --profile=testing -- ${CLIPPY_ARGS}

simple-bench: build .display_finish_time
	@time ./target/release/one_billion_challenge

time: simple-bench

fmt:
	@cargo +nightly fmt

pgo:
	@rm -rf /tmp/pgo-data
	@RUSTFLAGS="${BUILD_FLAGS} -Cprofile-generate=/tmp/pgo-data" cargo +nightly build --profile=pgo
	@make $(MEASUREMENT)
	@~/Downloads/LLVM/bin/llvm-profdata merge -o /tmp/pgo-data/merged.profdata /tmp/pgo-data
	@RUSTFLAGS="${BUILD_FLAGS} -Cprofile-use=/tmp/pgo-data/merged.profdata" cargo +nightly build --release

$(MEASUREMENT): .display_finish_time
	@./target/pgo/one_billion_challenge $(MEASUREMENT) > /dev/null

advanced-bench: build .display_finish_time
	@hyperfine ./target/release/one_billion_challenge --warmup 2

hyperfine: advanced-bench

flamegraph:
	@make .display_finish_time
	@cargo +nightly flamegraph --profile=flamegraph -o graph.svg

generate:
	@cd generator; cargo build -r
	@cp generator/target/release/generator data/generator
	@./data/generator --input=data/weather_stations.csv 1000000000 --output=data/measurements.txt
	@./data/generator --input=data/weather_stations.csv 1000000    --output=data/test_measurements.txt
	@./data/generator --input=data/weather_stations.csv 50000000   --output=data/bench_measurements.txt
